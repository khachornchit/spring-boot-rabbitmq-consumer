package net.putfirstthingsfirst.model;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

public class ReceiverMessage {

	@Autowired
	public RestTemplate restTemplate;

	@RabbitListener(queues = "#{queue1.name}")
	public void receiver1(String in) {
		receive(in, 1);
	}

	@RabbitListener(queues = "#{queue2.name}")
	public void receiver2(String in) {
		receive(in, 2);
	}

	private void receive(String in, int i) {

		Gson g = new Gson();
		SmsCommand sms = g.fromJson(in, SmsCommand.class);

		switch (i) {
		case 1:
			System.out.println(in + "<<>>" + i);
			restTemplate.postForObject("http://localhost:7002/amazon/subscribe", sms, String.class);
			break;

		case 2:
			System.out.println(in + "<<>>" + i);
			restTemplate.postForObject("http://localhost:7003/alibaba/subscribe", sms, String.class);
			break;

		default:
			System.out.println(in + "<<not match>>" + i);
			break;
		}
	}
}
