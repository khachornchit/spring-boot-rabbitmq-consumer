package net.putfirstthingsfirst.config;

import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import net.putfirstthingsfirst.model.ReceiverMessage;

@Configuration
public class ReceiverConfig {

	@Bean
	public TopicExchange exchange() {
		return new TopicExchange("Exchanging Message");
	}

	@Bean
	public Queue queue1() {
		return new AnonymousQueue();
	}

	@Bean
	public Queue queue2() {
		return new AnonymousQueue();
	}

	@Bean
	public Binding binding1() {
		return BindingBuilder.bind(queue1()).to(exchange()).with("*.amazon.*");
	}

	@Bean
	public Binding binding2() {
		return BindingBuilder.bind(queue1()).to(exchange()).with("*.alibaba.*");
	}
	
	@Bean
	public ReceiverMessage receivermsg() {
		return new ReceiverMessage();
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
